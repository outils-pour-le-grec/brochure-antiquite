# Introduction

L'objectif de ce projet est de réaliser une brochure présentant les sciences de l'Antiquité à l'intention des lycéens et, dans une moindre mesure, des collégiens.

Cette brochure comprend deux types de contenus :
- Des mini-interviews d'antiquisants du supérieur (double-page)
- Des doubles pages de présentation du supérieur

# Contribuer

Sur ce dépôt git, on trouvera :

- **Un fichier .sla** : c'est le fichier utilisé par Scribus, un logiciel libre de mise en page qui sert de support à la réalisation de ce guide.
- **Un dossier /Images** : Contient les illustrations utilisées au cours de la conception du document, dans l'idéal avec une source et une information sur la licence.
- **Un dossier /Motifs** : Contient les fichiers .svg (et leur export .png) utilisés pour les arrières-plans (notamment)
    - Le sous-dossier **FondDébouché1** est en gitignore (trop lourd, + de 100Mo)
- **Un dossier /Interviews** : Contiendra le schéma d'interview à faire compléter par les interviewés, ainsi que les réponses apportées par ceux-ci.
- **Un dossier /Export_pdf** : Contient la brochure exportée en pdf
- **Un dossier /Export_img** : Contient des pages exportées en img (à titre d'exemple) -> Ce dossier n'est pas synchronisé sur le git /!\ (trop lourd, inutile)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
