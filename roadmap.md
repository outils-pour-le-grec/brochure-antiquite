# Prochaines étapes

- [ ] Finir les pages intermédiaires
    - [ ] Page débouchés 2
    - [ ] Intro et sommaire (en cours)
    - [ ] FAQ et questions fréquentes
- [ ] Réaliser les interviews
    - [ ] Identifier les personnes concernées
    - [ ] Obtenir les réponses
    - [ ] Mettre en page les réponses
    - [ ] Réaliser les schémas
- [ ] Publication
    - [ ] Relecture générale
    - [ ] Financement de l'impression
    - [ ] Impression test
    - [ ] Impression définitive

# Le plan

*À l'heure actuelle, le livret est prévu pour un format 32 pages A5 (soit 16 pages A4)*

- [x]  0    : Couverture 1
- [x]  1    : Édito
- [x]  2    : Sommaire
- [x]  3-4  : Le supérieur
- [ ]  5-6  : Interview 1
- [ ]  7-8  : Interview 2
- [x]  9-10 : Les débouchés 1
- [ ] 11-12 : Interview 3
- [ ] 23-14 : Interview 4
- [x] 15-16 : Les différentes disciplines
- [ ] 17-18 : Interview 5
- [ ] 19-20 : Interview 6
- [ ] 21-22 : Les débouchés 2
- [ ] 23-24 : Interview 7
- [ ] 25-26 : Interview 8
- [ ] 27-28 : Interview 9
- [ ] 29-30 : FAQ ou Interview 10
- [x] 31    : Couverture 4
